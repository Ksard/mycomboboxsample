import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

//import MyStyle 1.0

ComboBox
{
    id: idCombo
    flat: true

    property int arrowSizeX: arrowSizeX
    property int arrowSizeY: arrowSizeY
    property int leftSpace:leftSpace
    property int rightSpace: rightSpace
    property int topSpace: topSpace
    property int bottomSpace: bottomSpace

    property int mainTopSpace: topSpace
    property int mainBottomSpace: bottomSpace

    property int rowSpace: rowSpace
    property int listSpace: listSpace
    property int rightRowSpace: rightRowSpace

    property string firstTextColor:firstTextColor
    property string secondTextColor:secondTextColor
    property string firstTextInverseColor:firstTextInverseColor
    property string secondTextInverseColor:secondTextInverseColor

    property font firstFont: firstFont
    property font secondFont: secondFont

    property  int firstTextAlign: firstTextAlign
    property  int secondTextAlign: secondTextAlign

    property string arrowColor: arrowColor

    property string backgroundColor:backgroundColor
    property string backgroundInverseColor:backgroundInverseColor
    property string borderColor: borderColor
    property string shadowColor: shadowColor

    property int shadowX: shadowX
    property int shadowY: shadowY
    property int shadowBlur: shadowBlur


    width: width
    height: height
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.top
    anchors.topMargin: height

    background: Rectangle
    {
        width: idCombo.width
        height: idCombo.height
        color: backgroundColor
        border.color: borderColor
    }

    delegate: ItemDelegate      //Здесь все про содержание и оформление ВСЕГО комбо-бокса
    {
        id: idDelegate
        width: idCombo.width
        leftPadding: leftSpace
        rightPadding: rightSpace


        contentItem: Row        //в т.ч. описание его строк
        {
            id: idRow
            spacing: rowSpace
            topPadding: topSpace
            bottomPadding: bottomSpace


            Text
            {
                id: firstText
                width: idRow.width / 2
                text: model.text1
                color: myMouse.containsMouse ? firstTextInverseColor : firstTextColor
                font: firstFont
                horizontalAlignment: firstTextAlign
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
            }

            Text
            {
                id: secondText
                width: idRow.width  - firstText.width - rightSpace - rightRowSpace
                text: model.text2
                color: myMouse.containsMouse ? secondTextInverseColor : secondTextColor
                font: secondFont
                horizontalAlignment: secondTextAlign
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
            }
            Rectangle
            {
                width: rightRowSpace
            }
        }
        background: Rectangle
        {
            MouseArea
            {
                id: myMouse
                anchors.fill: parent
                hoverEnabled: true

                onClicked:
                {
                    idCombo.currentIndex = index
                    idCombo.popup.close()
                }
            }
            color:  myMouse.containsMouse ? backgroundInverseColor : backgroundColor
        }



    }

    indicator: Canvas
    {
        id: myArrow
        width: arrowSizeX
        height: arrowSizeY
        x: idCombo.width - width - idCombo.rightPadding - rightSpace
        y: idCombo.topPadding + (idCombo.availableHeight - height) / 2
        contextType: "2d"

        Connections
        {
            target: idCombo
            onPressedChanged: myArrow.requestPaint()
            onCurrentIndexChanged: myArrow.requestPaint()

        }

        onPaint:
        {
            if(!idCombo.down)
            {
                context.reset();
                context.moveTo(width / 4, height / 4);
                context.lineTo(3 * width / 4, height / 4);
                context.lineTo(width / 2, 3 * height / 4);
            }
            else
            {
                context.reset();
                context.moveTo(width / 4, 3 * height / 4);
                context.lineTo(width / 2, height / 4);
                context.lineTo(3 * width / 4, 3 * height / 4);
            }

            context.closePath();
            context.fillStyle = arrowColor
            context.fill();
        }
    }

    contentItem: Row        //про отображение выбранного элемента на кнопке в виде описания его строки!
    {
        id: idCurrRow
        anchors.horizontalCenter: parent.horizontalCenter
        leftPadding: leftSpace
        rightPadding: idCombo.indicator.width + idCombo.spacing
        topPadding: mainTopSpace
        bottomPadding: mainBottomSpace

        Text
        {
            width: idCurrRow.width / 2
            text: model.get(currentIndex).text1
            color: firstTextColor
            font: firstFont
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            horizontalAlignment: firstTextAlign
        }
    }


    popup: Popup    //про открывшийся список
    {
        y: idCombo.height - 1
        width: idCombo.width
        implicitHeight: contentItem.implicitHeight
        padding: 1

        contentItem: ListView
        {
            id: idList
            clip: true
            spacing: listSpace

            implicitHeight: contentHeight
            currentIndex: idCombo.currentIndex
            model: idCombo.popup.visible ? idCombo.delegateModel : null
            ScrollIndicator.vertical: ScrollIndicator { }

        }

        background: Rectangle
        {
            id: idRect
            border.color: borderColor
            radius: 2

            DropShadow
            {
                anchors.fill: idRect
                horizontalOffset: shadowX
                verticalOffset: shadowY
                radius: shadowBlur
                samples: 1 + radius * 2
                color: shadowColor
                source: idList
            }

        }
    }


}
