import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0

Window
{
    id: idWin
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello ComboBox")


    ListModel
    {
        id: comboData
        ListElement { text1: "NewGold"; text2: "Kelvin Testnet" }
        ListElement { text1: "Kelvin"; text2: "Kelvin Testnet" }
        ListElement { text1: "Token 1"; text2: "Kelvin Testnet"  }
        ListElement { text1: "Token 2"; text2: "Kelvin Testnet"  }
        ListElement { text1: "Anheuser-Busch"; text2: "Marketnet" }
        ListElement { text1: "Genentech"; text2: "Marketnet" }

    }

    QtObject {
        id: theme
        property font comboBoxFirstFont: Qt.font({
                                                    family: "Calibri",  //Roboto not found...
                                                    weight: Font.Normal,
                                                    pixelSize: 16
                                                })

        property font comboBoxSecondFont: Qt.font({
                                                      family: "Calibri",  //Roboto not found...
                                                      weight: Font.Normal,
                                                      pixelSize: 12
                                                  })
    }

    MyComboBox
    {
        model: comboData
        firstFont: theme.comboBoxFirstFont
        secondFont: theme.comboBoxSecondFont
        width: 368
        height: 44

        arrowSizeX: 20
        arrowSizeY: 20

        leftSpace: 20
        rightSpace: 20
        topSpace: 3
        bottomSpace: 3
        mainTopSpace: 12
        mainBottomSpace: 12
        rowSpace: 20
        listSpace: 0 //10
        rightRowSpace: 44


        firstTextColor: "#070023"
        firstTextInverseColor: "#FFFFFF"
        secondTextColor: "#909D9D"
        secondTextInverseColor: "#FFFFFF"

        firstTextAlign: Text.AlignLeft
        secondTextAlign: Text.AlignRight

        arrowColor: "#070023"

        backgroundColor: "#FFFFFF"
        backgroundInverseColor: "#330F54"
        borderColor: "#F7F7F7"
        shadowColor: "grey"

        shadowX: 0
        shadowY: 9
        shadowBlur: 13

    }
}
